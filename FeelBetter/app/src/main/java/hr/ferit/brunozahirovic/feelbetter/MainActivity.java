package hr.ferit.brunozahirovic.feelbetter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static String SHARED_PREFS_NAME = "user";
    public static String USER_NAME_KEY = "userName";

    private TextView tvWelcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        View decorView = getWindow().getDecorView();
        setContentView(R.layout.activity_main);

        String userName = getSharedPreferencesUserName();
        userName="Bruno";
        if(userName == ""){
            launchFirstSetupScreen();
        }
        else{
            initMainScreen(userName);
        }
    }

    private void initMainScreen(String name) {

        tvWelcome = (TextView)findViewById(R.id.welcomeMessage);
        tvWelcome.setText("Welcome back, " + name);
    }

    private void launchFirstSetupScreen() {

    }

    private String getSharedPreferencesUserName() {
        SharedPreferences sharedPrefs = this.getSharedPreferences(SHARED_PREFS_NAME,MODE_PRIVATE);
        String userName = sharedPrefs.getString(USER_NAME_KEY,"").trim();
        return userName;
    }
}