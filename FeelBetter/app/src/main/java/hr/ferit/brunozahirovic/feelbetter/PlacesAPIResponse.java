package hr.ferit.brunozahirovic.feelbetter;

import java.util.List;

public class PlacesAPIResponse {
        
    public class Location{
        public double lat;
        public double lng;
    }

    public class Northeast{
        public double lat;
        public double lng;
    }

    public class Southwest{
        public double lat;
        public double lng;
    }

    public class Viewport{
        public Northeast northeast;
        public Southwest southwest;
    }

    public class Geometry{
        public Location location;
        public Viewport viewport;
    }

    public class Photo{
        public int height;
        public List<String> html_attributions;
        public String photo_reference;
        public int width;
    }

    public class PlusCode{
        public String compound_code;
        public String global_code;
    }

    public class Result{
        public String business_status;
        public Geometry geometry;
        public String icon;
        public String icon_background_color;
        public String icon_mask_base_uri;
        public String name;
        public List<Photo> photos;
        public String place_id;
        public PlusCode plus_code;
        public int rating;
        public String reference;
        public String scope;
        public List<String> types;
        public int user_ratings_total;
        public String vicinity;
    }

    public class Root{
        public List<Object> html_attributions;
        public List<Result> results;
        public String status;
    }
}
