package hr.ferit.brunozahirovic.feelbetter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkUtils {
    private static final String BASE_URL = "https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/";

    private static PokedexApi pokedexApi;

    public static PokedexApi getApiInterface(){

        if(pokedexApi == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            pokedexApi = retrofit.create(PokedexApi.class);

        }
        return pokedexApi;
    }
}
