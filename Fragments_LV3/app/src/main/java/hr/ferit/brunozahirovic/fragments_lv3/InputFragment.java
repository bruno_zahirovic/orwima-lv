package hr.ferit.brunozahirovic.fragments_lv3;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

public class InputFragment extends Fragment implements TextWatcher {
    private EditText mEditText;
    private Button mButton;
    private String mMessage = "";

    private ButtonClickListener mButtonClickListener;

    private static final String BUNDLE_MESSAGE = "message";

    public static InputFragment NewInstance(String message){
        InputFragment inputFragment = new InputFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_MESSAGE,message);
        inputFragment.setArguments(args);
        return inputFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditText = view.findViewById(R.id.editText);
        mButton = view.findViewById(R.id.button);
        setUpListeners();
    }

    private void setUpListeners() {
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mButtonClickListener != null){
                    mButtonClickListener.OnButtonClick(mMessage);
                }


            }
        });
        mEditText.addTextChangedListener(this);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof ButtonClickListener){
            this.mButtonClickListener = (ButtonClickListener)context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mButtonClickListener=null;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        mMessage = s.toString();
    }




}