package hr.ferit.brunozahirovic.fragments_lv3;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.telephony.ims.ImsMmTelManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.nio.BufferUnderflowException;

public class TextViewFragment extends Fragment {
    private TextView mTextView;


    private static final String BUNDLE_MESSAGE = "message";

    public static TextViewFragment NewInstance(String message){
        TextViewFragment textViewFragment = new TextViewFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_MESSAGE,message);
        textViewFragment.setArguments(args);
        return textViewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_text_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTextView = view.findViewById(R.id.textView);
    }

    public void setMessage(String message){
        mTextView.setText(message.trim().isEmpty() ? "..." : message);
    }



}