package hr.ferit.brunozahirovic.fragments_lv3;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;


public class ModularFragment extends Fragment {
    private  ImageView mImageView;
    private  TextView mTextView;
    private final String mode;

    public ModularFragment(String mode){
        this.mode=mode;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_modular, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mImageView = view.findViewById(R.id.imageViewModular);
        mTextView = view.findViewById(R.id.textViewModular);

        switch  (mode) {
            case "TextView":
                mTextView.setVisibility(View.VISIBLE);
                break;
            case "ImageView":
                mImageView.setVisibility(View.VISIBLE);
                break;
            default:
                mTextView.setText("??????");
                mTextView.setVisibility(View.VISIBLE);
        }

    }


}