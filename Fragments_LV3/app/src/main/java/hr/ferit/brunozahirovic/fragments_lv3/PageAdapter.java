package hr.ferit.brunozahirovic.fragments_lv3;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class PageAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 4;

    private ArrayList<Fragment> fragments = new ArrayList<>();

    public PageAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = InputFragment.NewInstance("TAB #" + (position+1));
                break;
            case 1:
                fragment = TextViewFragment.NewInstance("TAB #" + (position+1));
                break;
            case 2:
                fragment =  new ModularFragment("TextView");
                break;
            case 3:
                fragment = new ModularFragment("ImageView");
        }
        fragments.add(fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "#" + (position + 1);
    }

}
