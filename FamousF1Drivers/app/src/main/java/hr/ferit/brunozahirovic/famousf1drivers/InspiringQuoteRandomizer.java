package hr.ferit.brunozahirovic.famousf1drivers;

import java.util.ArrayList;
import java.util.Random;

public class InspiringQuoteRandomizer {
    private ArrayList<String> randomQuotes;
    private Random random;

    public InspiringQuoteRandomizer(ArrayList<String> randomQuotes){
        this.randomQuotes = randomQuotes;
        random = new Random();
    }

    public String ReturnRandomQuote(){
        int randomQuoteID=random.nextInt(this.randomQuotes.size());
        String randomQuote=randomQuotes.get(randomQuoteID);
        return randomQuote;
    }
}
