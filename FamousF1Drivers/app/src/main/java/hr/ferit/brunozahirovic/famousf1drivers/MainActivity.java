package hr.ferit.brunozahirovic.famousf1drivers;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnInspiration;
    private TextView tvTop3;
    private EditText etDescriptionChange;
    private Button btnEditDescription;
    private RadioGroup rgChoosePerson;
    private RadioButton rbChoosePerson;
    private ImageView ivFirstPerson;
    private ImageView ivSecondPerson;
    private ImageView ivThirdPerson;

    private ArrayList<String> peopleQuotes;
    private InspiringQuoteRandomizer quoteRandomizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Initialize();

        btnEditDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rbID = rgChoosePerson.getCheckedRadioButtonId();
                rbChoosePerson=findViewById(rbID);

                switch (rbChoosePerson.getId()) {
                    case R.id.rbFirstPerson :
                        ((TextView)findViewById(R.id.tvFirstPersonDescription)).setText(etDescriptionChange.getText().toString());
                        break;
                    case R.id.rbSecondPerson:
                        ((TextView)findViewById(R.id.tvSecondPersonDescription)).setText(etDescriptionChange.getText().toString());
                        break;
                    case R.id.rbThirdPerson:
                        ((TextView)findViewById(R.id.tvThirdPersonDescription)).setText(etDescriptionChange.getText().toString());
                        break;
                }

            }
        });
        btnInspiration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayToast(quoteRandomizer.ReturnRandomQuote());
            }
        });

        ivFirstPerson.setOnClickListener(this);
        ivSecondPerson.setOnClickListener(this);
        ivThirdPerson.setOnClickListener(this);



    }

    private void Initialize(){
        btnEditDescription = (Button) findViewById(R.id.btnEditDescription);
        btnInspiration = (Button) findViewById(R.id.btnInspiration);
        tvTop3 = (TextView) findViewById(R.id.tvTop3);
        etDescriptionChange = (EditText) findViewById(R.id.etDescriptionChange);
        rgChoosePerson = (RadioGroup) findViewById(R.id.rgChoosePerson);
        ivFirstPerson = (ImageView)findViewById(R.id.ivFirstPerson);
        ivSecondPerson = (ImageView)findViewById(R.id.ivSecondPerson);
        ivThirdPerson = (ImageView)findViewById(R.id.ivThirdPerson);


        peopleQuotes = new ArrayList<String>();

        peopleQuotes.add(getString(R.string.firstPersonQuote));
        peopleQuotes.add(getString(R.string.secondPersonQuote));
        peopleQuotes.add(getString(R.string.thirdPersonQuote));

        quoteRandomizer = new InspiringQuoteRandomizer(peopleQuotes);
    }
    public void displayToast(String message){

        Toast.makeText(this,message, LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        v.setVisibility(View.GONE);
    }
}