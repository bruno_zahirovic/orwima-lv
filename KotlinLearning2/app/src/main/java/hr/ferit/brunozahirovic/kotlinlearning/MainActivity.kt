package hr.ferit.brunozahirovic.kotlinlearning

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView = findViewById<TextView>(R.id.tvHello) as TextView
        textView.text = "Joe"
        textView.textSize = 25F
        println("Joe")
        val userName: String = "Joeseph"
        val textSize: Float = 45F
        textView.textSize = textSize
        printString(userName)
        25.toString()
    }

    private fun printString(string: String) {
        println("User: $string")

    }
}