package com.example.clickme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class HomeScreen extends AppCompatActivity {


    private Button startButton;
    private TextView welcomeTextView;
    private EditText nameEditText;

    public String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        startButton = (Button) findViewById(R.id.startButton);
        welcomeTextView = (TextView) findViewById(R.id.welcomeTextView);
        nameEditText = (EditText) findViewById(R.id.nameEditText);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(nameEditText.getText().toString() == ""){
                        throw new IllegalArgumentException();
                    }
                    name = nameEditText.getText().toString();
                    switchActivity();
                }catch (IllegalArgumentException e){
                    System.out.println("No argument entered");
                    System.exit(-1);
                }

            }
        });

    }
    public void switchActivity(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}