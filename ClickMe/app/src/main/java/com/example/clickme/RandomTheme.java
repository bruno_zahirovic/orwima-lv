package com.example.clickme;

import android.graphics.Color;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Random;

public class RandomTheme {
    private final int[] textColor;
    private final int[] backgroundColor;
    private int randInt;

    public RandomTheme(){
        textColor = new int[] {Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW};
        backgroundColor = new int[] {Color.BLUE, Color.YELLOW, Color.RED, Color.GREEN};
    }

    public void RandomizeButtonTheme(ArrayList<Button> buttons){
        Random random = new Random();
        randInt = random.nextInt(textColor.length);

        for (Button btn: buttons) {
            btn.setTextColor(textColor[randInt]);
            btn.setBackgroundColor(backgroundColor[randInt]);
        }
    }

    public void RandomizeTextViewColor(ArrayList<TextView> textViews){
        for (TextView textView: textViews) {
            textView.setTextColor(textColor[randInt]);
        }
    }
}
