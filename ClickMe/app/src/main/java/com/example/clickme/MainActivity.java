package com.example.clickme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowInsets;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button clickBtn;
    private TextView clickCounterTextView;
    private Button exitBtn;
    private Switch switch1;
    private Button clickBtn2;
    RandomTheme randomTheme;
    ArrayList<Button> buttons;
    ArrayList<TextView> textViews;



    public static final long COUNTER = 0;
    private long ctr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        randomTheme = new RandomTheme();
        buttons  = new ArrayList<Button>();
        textViews = new ArrayList<TextView>();
        switch1 = (Switch) findViewById(R.id.switch1);
        clickCounterTextView = (TextView) findViewById(R.id.clickCounterTextView);
        clickBtn = (Button) findViewById(R.id.clickBtn);
        clickBtn2 = (Button) findViewById(R.id.clickBtn2);

        buttons.add(clickBtn);
        buttons.add(clickBtn2);
        textViews.add(clickCounterTextView);

        LoadData();
        clickCounterTextView.setText(ctr+"");

        clickBtn.setOnClickListener(this);

        exitBtn = (Button) findViewById(R.id.exitBtn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData();
                System.exit(0);
            }
        });
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    clickBtn2.setVisibility(View.VISIBLE);
                    clickBtn2.setOnClickListener(MainActivity.this::onClick);
                }
                else{
                    clickBtn2.setVisibility(View.INVISIBLE);
                }
            }
        });

        LoadData();
        clickCounterTextView.setText(ctr+"");
    }

    @Override
    protected void onDestroy() {
        SaveData();
        super.onDestroy();
    }

    public void SaveData() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putLong("COUNTER",ctr);
        editor.commit();

        Toast.makeText(this,"Goodbye",Toast.LENGTH_SHORT).show();
    }

    public void LoadData(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        ctr = sharedPreferences.getLong("COUNTER",0);

    }


    @Override
    public void onClick(View v) {
        ctr++;
        if(ctr%100 == 0){
            randomTheme.RandomizeButtonTheme(buttons);
            randomTheme.RandomizeTextViewColor(textViews);
        }
        clickCounterTextView.setText(ctr+"");
    }
}

