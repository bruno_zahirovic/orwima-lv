package hr.ferit.brunozahirovic.attendance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RecyclerAdapter.OnItemClickListener {
    private Button btnAdd;
    private EditText etName;
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;

    private List<String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etName = findViewById(R.id.etName);
        btnAdd = findViewById(R.id.btnAdd);
        recyclerAdapter = new RecyclerAdapter();

        setupRecycler();
        setupData();
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etName.getText().toString().isEmpty()) {
                    recyclerAdapter.addNewItem(etName.getText().toString(), 0);
                }
            }
        });



    }

    public void setupRecycler(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerAdapter);

        recyclerAdapter.setOnItemClickListner(this);
    }

    public void setupData(){
        data = new ArrayList<>();

    }

    public void addItem(View view){
        recyclerAdapter.addNewItem(etName.getText().toString(),data.size());
    }



    @Override
    public void onItemClick(int position) {
        recyclerAdapter.removeItem(position);
    }
}