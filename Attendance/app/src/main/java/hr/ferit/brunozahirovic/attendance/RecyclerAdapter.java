package hr.ferit.brunozahirovic.attendance;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter  extends RecyclerView.Adapter<RecyclerAdapter.NameViewHolder> {
    private List<String> data = new ArrayList<>();
    private static String TAG = "RecyclerAdapter";
    private OnItemClickListener mListener;

    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG,"onCreateViewHolder()");
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        return new NameViewHolder(listItem,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NameViewHolder holder, int position) {
        Log.d(TAG,"onBindViewHolder()");
        holder.setName(data.get(position));
    }

    @Override
    public int getItemCount() {
        Log.d(TAG,"getItemCount()");
        return this.data.size();
    }

    public void addData(List<String> data){
        Log.d(TAG,"addData()");
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void addNewItem(String name,int position){
        Log.d(TAG,"addNewItem()");
        if(data.size()>=position){
            data.add(position,name);
            notifyDataSetChanged();
        }
    }


    public void removeItem(int position){
        Log.d(TAG,"removeItem()");
        if(data.size()>position){
            data.remove(position);
            notifyDataSetChanged();
        }
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListner(OnItemClickListener listner){
        mListener=listner;
    }

    public static class NameViewHolder extends RecyclerView.ViewHolder{
        private TextView tvName;
        private Button btnRemove;


        public NameViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tvName);
            btnRemove=itemView.findViewById(R.id.btnRemove);
            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }
        public void setName(String name){
            Log.d(TAG,"NameViewHolder:setName()");
            tvName.setText(name);
        }

    }

}
